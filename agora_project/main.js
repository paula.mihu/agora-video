import AgoraRTC from "agora-rtc-sdk"

let options =
{
    // Pass your App ID here.
    appId: '9ac9dd21f56a43db8874eda2ceb02e0a',
    // Set the channel name.
    channel: 'Paula',
    // Pass your temp token here.
    token: '0069ac9dd21f56a43db8874eda2ceb02e0aIADS6PUCEW3bQolW4nYCT8KPtA6GD2xuUK+Bs+zxuu4Mu253bNwAAAAAEACvrbVs3M86YgEAAQDdzzpi',
    // Set the user ID.
    uid: '0',
};

async function startBasicCall()
{
    // Create an instance of the Agora Engine
    const agoraEngine = AgoraRTC.createClient({ mode: "rtc", codec: "vp8" });
    // Inititalize the Client object
     // Create a local stream from the video captured by a camera.
     var localStream = AgoraRTC.createStream(
        {
            streamID: options.uid,
            audio: true,
            video: true,
        });
        agoraEngine.init(options.appId, function ()
        {
            console.log("Agora Engine initialized");
        },
        function (err)
        {
            console.log("[ERROR] : Agora Engine init failed", err);
        });
        agoraEngine.on('stream-published', function (evt)
        {
            console.log("Publish local stream successfully");
        });
        // Remove the corresponding view when a remote stream is removed
        agoraEngine.on("stream-removed", function (evt)
        {
            console.log("Removing stream...");
            let stream = evt.stream;
            let streamId = String(stream.getId());
            stream.close();
            removeVideoDiv(streamId);
        });
        // Remove the corresponding view when a remote user leaves the channel
        agoraEngine.on("peer-leave", function (evt)
        {
            console.log("peer-left");
            let stream = evt.stream;
            let streamId = String(stream.getId());
            stream.close();
            removeVideoDiv(streamId);
        });
        const remotePlayerContainer = document.createElement("div");
        var localPlayerContainer = document.getElementById('localStream');
        window.onload = function ()
        {
            document.getElementById("join").onclick = async function ()
            {
                // Join an RTC channel.
                await agoraEngine.join(options.token, options.channel, null, (uid) =>
                {
                    console.log("join success", uid);
                },
                (e) =>
                {
                    console.log("join failed", e);
                });
                // Dynamically create a container in the form of a DIV element for playing the local video track
                if (localPlayerContainer==null)
                {
                    localPlayerContainer=document.createElement("div");
                    // Specify the ID of the DIV container
                    localPlayerContainer.id = "localStream";
                    localPlayerContainer.textContent = "Local user: " + options.uid;
                    localPlayerContainer.style.width = "640px";
                    localPlayerContainer.style.height = "480px";
                    localPlayerContainer.style.padding = "25px";
                }
                document.body.append(localPlayerContainer);
                // Initialize the local stream
                localStream.init(() =>
                {
                    // Pass the DIV container ID and the SDK dynamically creates a player in the container
                    localStream.play(localPlayerContainer.id);
                    // Publish the local stream
                    agoraEngine.publish(localStream, handleError);
                    console.log("publish success!");
                }, handleError);
                // Subscribe to the remote stream when it is added
                agoraEngine.on('stream-added', e =>
                {
                    console.log("Stream_added called");
                    agoraEngine.subscribe(e.stream, function (err)
                    {
                        console.log("[ERROR] : subscribe stream failed", err);
                    });
                });
                // Play the remote stream when it is subscribed
                agoraEngine.on("stream-subscribed", e =>
                {
                    console.log("subscribe success " + e.stream.getId());
                    // Dynamically create a container in the form of a DIV element for playing the remote video track.
                    // Specify the ID of the DIV container.
                    remotePlayerContainer.id = e.stream.getId();
                    remotePlayerContainer.textContent = "Remote stream " + e.stream.getId();
                    remotePlayerContainer.style.width = "640px";
                    remotePlayerContainer.style.height = "480px";
                    remotePlayerContainer.style.padding = "25px";
                    document.body.append(remotePlayerContainer);
                    e.stream.play(remotePlayerContainer.id);
                    console.log("Remote video played");
                });
            }
            document.getElementById('leave').onclick = async function ()
            {
                // Close the local stream
                if (localStream != null)
                localStream.close();
                options.role='';
                removeVideoDiv(remotePlayerContainer.id);
                removeVideoDiv(localPlayerContainer.id);
                // Leave the channel
                agoraEngine.leave();
                console.log("User leaves channel");
                agoraEngine.unpublish(localStream, function (err)
                {
                    console.log("Unpublish local stream failed" + err);
                });
                // Refresh the page for reuse
                window.location.reload();
            }

        }
    }

startBasicCall();
// Handle errors
let handleError = function (err)
{
    console.log("Error: ", err);
};
// Remove the video stream from the container.
function removeVideoDiv(elementId)
{
    console.log("Removing "+ elementId+"Div");
    let Div = document.getElementById(elementId);
    if (Div)
    {
        Div.remove();
    }
};
