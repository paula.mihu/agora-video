

:imagesdir: {rootdir}/images
:URL_ROOT: https://docs.agora.io/en

//                       Agora components                     //

:COMPANY: Agora
:AGORA_BACKEND: {COMPANY}
:CLIENT: app
:CONSOLE: {COMPANY} Console
:TOKEN: token

ifeval::[ "{platform}" == "unity"]
:CLIENT: game
endif::[]

//                       Real-Time Engagement Core Products               //

:ENGINE: {COMPANY} Engine
:RTE: Real-Time Engagement
:RTEC: {RTE} Core
:RTES: {RTE} SDK

// Audio/Video
:VIDEO: Video Calling
:AUDIO: Voice Calling
:ILS: Interactive Live Streaming
:ILSS: Interactive Live Streaming Standard
:ILSP: Interactive Live Streaming Premium
:VSDK: Video SDK

:AV: Audio/Video
:AV_URL: {URL_ROOT}/video


// Messaging
:MESS: Real-Time Messaging
:SIG: {MESS}
:CHAT: {COMPANY} Chat
:PS: Pub-Sub
:PUSH: Push Notifications

// Acceleration

:ACC: Acceleration
:ACCM: Media Accelerator
:ACCG: Global Accelerator

//                       Real-Time Engagement Extensions                //

:RTEE: {COMPANY}  Extensions
:AA: {COMPANY} Analytics
:WHITE: Interactive Whiteboard
:EM: {RTEE} Marketplace
:MS: Media Services
:REC: Recording
:MOD: Moderation
:CP: Cloud Proxy
:RTMPC: RTMP Converter
:TRANS: Transcoding



//                       Tools                 //

:AB: App Builder
:UIK: UIKit
:FC: Flexible Classroom


//                       Platforms                     //

:WEB: Web
:IOS: iOS
:MAC: MacOS
:AND: Android
:WIN: Windows

:ELE: Electron
:UNI: Unity
:FLU: Flutter
:REA: React Native
:COC: Cocos Creator
:CO2-AND: Cocos2d-x-Android
:SS: Server side



//                       Roles                     //

:ATW: Technical Writer
:ADR: DevReller
:ADEV: Developer
:AQA: Tester
:APO: Product Owner


//                       Tech Writer things               //
:attribute-missing: warn
:REPO_URL: https://gitlab.com/billy-the-fish/asciidoc-test

:doctype: book

:AGORA-DOCS-URL: https://docs.agora.io/en

:AGORA-PLATFORM-FOLDER: Agora%20Platform

:GET-STARTED-LINK: link:{rootdir}/{PRODUCT-FOLDER}/{GET-STARTED-PREFIX}_{platform}.adoc[Get Started with {PRODUCT}]

:TOKEN-AUTH-LINK: link:{rootdir}/{PRODUCT-FOLDER}/token_server_{platform}.adoc[Authenticate Your Users with Tokens]

:APP-ID-LINK: {AGORA-DOCS-URL}/{AGORA-PLATFORM-FOLDER}/get_appid_token#get-the-app-id[App ID]

:APP-CERTIFICATE-LINK: {AGORA-DOCS-URL}/{AGORA-PLATFORM-FOLDER}/get_appid_token#get-the-app-certificate[App Certificate]

:DEMO-BASIC-VIDEO-CALL-URL: https://webdemo.agora.io/agora-web-showcase/examples/Agora-Web-Tutorial-1to1-Web/

:DEMO-PAGE-LINK: {DEMO-BASIC-VIDEO-CALL-URL}[{COMPANY} web demo]
